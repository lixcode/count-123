#include <iostream>

using namespace std;

int main() {
    std::string input;

    cout << "Enter bit sequence: ";
    cin >> input;

    input.push_back('0');

    int max_1   = 0;
    int count_1 = 0;

    int max_2   = 0;
    int count_2 = 0;

    int count_3 = 0;

    for (auto ch : input) {
        if (ch == '1') {
            count_1++;
        }
        else {

            max_1 = max(max_1, count_1);
            max_2 = max(max_2, count_2 + count_1);

            count_3 += max(0, min(count_1, 1));
            count_2 = count_1;
            count_1 = 0;
        }
    }

    if (count_3 >= 2) {
        max_1++;
    }

    if (count_3 >= 3) {
        max_2++;
    }

    cout << "max is " << max(max_1, max_2) << endl;

    return 0;
}
