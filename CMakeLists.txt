cmake_minimum_required(VERSION 3.5)

project(count-1110 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(count-1110 main.cpp)
